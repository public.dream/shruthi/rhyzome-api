// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"

	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

var ConfigFiles = []string{"/etc/rhyzome-api.json", "rhyzome-api.json"}

// Config describes all configurable keys
type Config struct {
	HTTPBind         string                   `json:"http_bind"` // HTTPBind is the address (and port) to bind the REST server to. It should be accessible via the internet, via an https proxy
	GRPCBind         string                   `json:"grpc_bind"`
	DB               string                   `json:"db"` // DB is the postgres database connection string. For example, "dbname=exampledb user=webapp password=webapp"
	PKI              PKI                      `json:"pki"`
	Keycloak         KeycloakConfig           `json:"keycloak"`
	DisableAuthz     bool                     `json:"disable_authz"`
	ProvisioningInfo rhyzome.ProvisioningInfo `json:"provisioning_info"` // ProvisioningInfo is publicly available and used to join new nodes to the cluster
}

type PKI struct {
	Cert string `json:"cert"`
	Key  string `json:"key"`
	CA   string `json:"ca"`
}

type KeycloakConfig struct {
	JWKS          string `json:"jwks"`
	ResourceScope string `json:"resource_scope"` // ResourceScope is the oauth client permissions are assigned from
}

var (
	// C stores the actual configured values
	C Config

	// Defaults are the default values for each config option, set in the Load() function
	Defaults = Config{
		HTTPBind:     ":8080",
		GRPCBind:     ":9090",
		Keycloak:     KeycloakConfig{JWKS: "https://auth.entanglement.garden/auth/realms/entanglement.garden/protocol/openid-connect/certs"},
		DisableAuthz: true,
	}
)

// Load loads the configuration off the disk
func Load() {
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		CallerPrettyfier:          logPrettifier,
	})

	C = Defaults
	for _, filename := range ConfigFiles {
		jsonFile, err := os.Open(filename)
		if err != nil {
			if !os.IsNotExist(err) {
				log.Printf("error opening config file %s: %s", filename, err.Error())
			}
			continue
		}
		defer jsonFile.Close()

		byteValue, err := ioutil.ReadAll(jsonFile)
		if err != nil {
			log.Printf("error reading config file %s: %s", filename, err.Error())
			continue
		}

		err = json.Unmarshal(byteValue, &C)
		if err != nil {
			log.Printf("error parsing config file %s: %s", filename, err.Error())
			continue
		}

		log.Infof("successfully read config from %s", filename)
		log.Debugf("config as loaded: %+v", C)
	}
}

func logPrettifier(f *runtime.Frame) (string, string) {
	filenameParts := strings.SplitN(f.File, "git.callpipe.com/entanglement.garden/rhyzome-api/", 2)
	filename := f.File
	if len(filenameParts) > 1 {
		filename = fmt.Sprintf("%s:%d", filenameParts[1], f.Line)
	}
	s := strings.Split(f.Function, ".")
	funcname := fmt.Sprintf("%s()", s[len(s)-1])
	return funcname, filename
}
