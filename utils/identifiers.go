// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package utils

import (
	"math/rand"
	"time"
)

const (
	generatedIDSize = 5
)

var (
	charset     = []rune("abcdefghijklmnopqrstuvwxyz")
	charsetSize = len(charset)
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func GenerateID() (id string) {
	for len(id) < generatedIDSize {
		id = id + string(charset[rand.Int()%charsetSize])
	}
	return id
}
