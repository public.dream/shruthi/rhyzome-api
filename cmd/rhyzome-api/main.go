// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver"
	"git.callpipe.com/entanglement.garden/rhyzome-api/httpapi"
)

var signals = make(chan os.Signal, 1)

func main() {
	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)
	config.Load()

	go httpapi.Serve()
	go grpcserver.ListenAndServe()

	for {
		signal := <-signals
		log.Debug("Received signal", signal)
		switch signal {
		case syscall.SIGHUP:
			config.Load()
			grpcserver.ReloadCertificates()
		case syscall.SIGINT:
			httpapi.Shutdown(context.Background())
			grpcserver.Shutdown()
			return
		}
	}
}
