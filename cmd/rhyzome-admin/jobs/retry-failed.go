// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package jobs

import (
	"database/sql"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
)

var (
	retryFailedJobsCmd = &cobra.Command{
		Use:   "retry-failed",
		Short: "retry all failed jobs",
		RunE: func(cmd *cobra.Command, _ []string) error {
			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				return err
			}
			defer dbConn.Close()
			queries := db.New(dbConn)

			failedJobs, err := queries.GetFailedJobs(cmd.Context())
			if err != nil {
				return err
			}
			log.Infof("retrying %d failed jobs", len(failedJobs))
			for _, job := range failedJobs {
				log.WithField("job_id", job.JobID).Info("retrying job")
				retryJobID := utils.GenerateID()
				err = queries.CreateJob(cmd.Context(), db.CreateJobParams{
					JobID:          retryJobID,
					AssignedWorker: job.AssignedWorker,
					State:          db.JobStateQUEUED,
					StateText:      "RETRYING",
					JobType:        job.JobType,
					JobData:        job.JobData,
				})
				if err != nil {
					return err
				}
				log.Debug("created new job", "job_id", retryJobID)

				err = queries.DeleteJob(cmd.Context(), job.JobID)
				if err != nil {
					return err
				}
				log.Debug("deleted old job")
			}
			return nil
		},
	}
)

func init() {
	JobsCmd.AddCommand(retryFailedJobsCmd)
}
