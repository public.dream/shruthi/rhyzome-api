// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package jobs

import (
	_ "github.com/lib/pq"
	"github.com/spf13/cobra"
)

var (
	JobsCmd = &cobra.Command{
		Use:   "jobs",
		Short: "manage jobs",
	}
)
