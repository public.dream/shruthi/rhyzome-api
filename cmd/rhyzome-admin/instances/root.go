// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"github.com/spf13/cobra"
)

var (
	InstancesCmd = &cobra.Command{
		Use:     "instance",
		Aliases: []string{"instances"},
		Short:   "manage instances",
		Run:     listInstancesCmd.Run,
	}
)
