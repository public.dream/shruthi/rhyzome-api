// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"database/sql"
	"encoding/json"
	"os"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

var (
	listInstancesCmd = &cobra.Command{
		Use:   "list",
		Short: "list all instances",
		Run: func(cmd *cobra.Command, _ []string) {
			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				log.Fatal("error connecting to database:", err)
			}
			defer dbConn.Close()

			queries := db.New(dbConn)

			tenants, err := queries.ListInstances(cmd.Context(), db.DefaultTenantName)
			if err != nil {
				log.Fatal("error listing tenants: ", err)
			}
			err = json.NewEncoder(os.Stdout).Encode(tenants)
			if err != nil {
				log.Fatal("error encoding output:", err)
			}
		},
	}
)

func init() {
	InstancesCmd.AddCommand(listInstancesCmd)
}
