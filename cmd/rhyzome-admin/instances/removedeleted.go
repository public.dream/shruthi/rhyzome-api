// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"database/sql"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

var (
	removeDeletedInstancesCmd = &cobra.Command{
		Use:   "remove-deleted",
		Short: "remove deleted instances",
		RunE: func(cmd *cobra.Command, _ []string) error {
			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				log.Fatal("error connecting to database:", err)
			}
			defer dbConn.Close()

			queries := db.New(dbConn)

			instances, err := queries.ListInstances(cmd.Context(), db.DefaultTenantName)
			if err != nil {
				log.Fatal("error listing instances: ", err)
			}
			i := 0
			for _, instance := range instances {
				if instance.State == db.InstanceStateDELETED {
					err = queries.DeleteInstance(cmd.Context(), instance.ResourceID)
					if err != nil {
						log.Error("error deleting instance")
						return err
					}

					err = queries.DeleteResource(cmd.Context(), instance.ResourceID)
					if err != nil {
						log.Error("error deleting instance resource")
						return err
					}

					err = queries.DeleteVolume(cmd.Context(), instance.RootVolume)
					if err != nil {
						log.Error("error deleting volume")
						return err
					}

					err = queries.DeleteResource(cmd.Context(), instance.RootVolume)
					if err != nil {
						log.Error("error deleting root volume resource")
						return err
					}
					i++
				}
			}
			log.Info("cleaned ", i, " instances")
			return nil
		},
	}
)

func init() {
	InstancesCmd.AddCommand(removeDeletedInstancesCmd)
}
