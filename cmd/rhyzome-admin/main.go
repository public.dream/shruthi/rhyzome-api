// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/cmd/rhyzome-admin/debug"
	"git.callpipe.com/entanglement.garden/rhyzome-api/cmd/rhyzome-admin/instances"
	"git.callpipe.com/entanglement.garden/rhyzome-api/cmd/rhyzome-admin/jobs"
	"git.callpipe.com/entanglement.garden/rhyzome-api/cmd/rhyzome-admin/nodes"
	"git.callpipe.com/entanglement.garden/rhyzome-api/cmd/rhyzome-admin/tenant"
	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
)

var rootCmd = &cobra.Command{
	Use:   "rhyzome-admin",
	Short: "Rhyzome administrative command line tool",
	Long:  `A CLI for preforming administrative actions to rhyzome`,
}

func main() {
	config.Load()
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.AddCommand(debug.DebugCmd)
	rootCmd.AddCommand(instances.InstancesCmd)
	rootCmd.AddCommand(jobs.JobsCmd)
	rootCmd.AddCommand(nodes.NodesCmd)
	rootCmd.AddCommand(tenant.TenantCmd)
}
