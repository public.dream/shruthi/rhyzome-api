// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package tenant

import (
	"database/sql"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

var (
	createTenantCmd = &cobra.Command{
		Use:   "create [name]",
		Short: "dcreate a new tenant",
		PreRun: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				if err := cmd.Help(); err != nil {
					log.Fatal("unexpected error rendering help message:", err)
				}
				log.Fatal("exactly 1 argument expected")
			} else if len(args[0]) < 3 || len(args[0]) > 20 {
				log.Fatal("tenant name must be 3-20 characters")
			} else {
				name = args[0]
			}
		},
		Run: func(cmd *cobra.Command, _ []string) {
			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				log.Fatal("error connecting to database:", err)
			}
			defer dbConn.Close()

			tenantsTable := db.New(dbConn)

			if err := tenantsTable.CreateTenant(cmd.Context(), name); err != nil {
				log.Fatal("error creating tenant: ", err)
			}
			log.WithField("tenant", name).Info("tenant created")
		},
	}
)

func init() {
	TenantCmd.AddCommand(createTenantCmd)
}
