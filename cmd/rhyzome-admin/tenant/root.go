// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package tenant

import (
	"github.com/spf13/cobra"
)

var (
	name      string
	TenantCmd = &cobra.Command{
		Use:     "tenant",
		Aliases: []string{"tenants"},
		Short:   "manage tenants",
		Run:     listTenantCmd.Run,
	}
)
