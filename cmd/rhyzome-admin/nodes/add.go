// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package nodes

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
)

var (
	hostname   string
	available  bool
	addNodeCmd = &cobra.Command{
		Use:   "add [hostname]",
		Short: "add a rhyzome node",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				err := cmd.Help()
				if err != nil {
					log.Fatal("error showing help: ", err)
					return err
				}
				return errors.New("no args")
			}
			hostname = args[0]
			return nil
		},
		RunE: func(cmd *cobra.Command, _ []string) error {
			id := utils.GenerateID()
			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				return err
			}
			defer dbConn.Close()

			queries := db.New(dbConn)
			err = queries.CreateResource(cmd.Context(), db.CreateResourceParams{ResourceID: id, Tenant: db.DefaultTenantName})
			if err != nil {
				return err
			}

			_, err = queries.AddNode(cmd.Context(), db.AddNodeParams{ResourceID: id, Hostname: hostname, Available: available})
			if err != nil {
				return err
			}
			return nil
		},
	}
)

func init() {
	NodesCmd.AddCommand(addNodeCmd)
	addNodeCmd.Flags().BoolVarP(&available, "available", "a", false, "set this node as available right away")
}
