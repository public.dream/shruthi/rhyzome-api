// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package nodes

import (
	"database/sql"
	"encoding/json"
	"os"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

var (
	NodesCmd = &cobra.Command{
		Use:   "nodes",
		Short: "configure rhyzome instances",
		Run: func(cmd *cobra.Command, _ []string) {
			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				log.Fatal("error connecting to database:", err)
			}
			defer dbConn.Close()

			queries := db.New(dbConn)
			nodes, err := queries.GetNodes(cmd.Context())
			if err != nil {
				log.Fatal("error querying nodes: ", err)
				return
			}
			err = json.NewEncoder(os.Stdout).Encode(nodes)
			if err != nil {
				log.Fatal("error encoding nodes as JSON: ", err)
				return
			}

		},
	}
)
