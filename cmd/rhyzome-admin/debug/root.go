// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package debug

import (
	"github.com/spf13/cobra"
)

var (
	DebugCmd = &cobra.Command{
		Use:   "debug",
		Short: "debug the service",
	}
)
