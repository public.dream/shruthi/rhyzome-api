// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package debug

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
)

var (
	debugEventsCmd = &cobra.Command{
		Use:   "events",
		Short: "debug database events",
		Run: func(cmd *cobra.Command, _ []string) {
			_, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				panic(err)
			}

			reportProblem := func(ev pq.ListenerEventType, err error) {
				if err != nil {
					log.Fatal("error receiving from database event stream:", err)
				}
			}

			listener := pq.NewListener(config.C.DB, 10*time.Second, time.Minute, reportProblem)
			for _, channel := range []string{"jobs", "tenants", "instances"} {
				err = listener.Listen(channel)
				if err != nil {
					panic(err)
				}
			}

			log.Info("Start monitoring PostgreSQL...")
			for {
				select {
				case n := <-listener.Notify:
					fmt.Println(n.Extra)
				case <-time.After(10 * time.Second):
					log.Debug("Received no events for 10 seconds, pinging database")
					go func() {
						err := listener.Ping()
						if err != nil {
							log.Fatal("error connecting to database:", err)
						} else {
							log.Debug("pong from database")
						}
					}()
				}
			}
		},
	}
)

func init() {
	DebugCmd.AddCommand(debugEventsCmd)
}
