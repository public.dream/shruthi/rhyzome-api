// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package debug

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"os"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
)

var (
	jobType      string
	worker       string
	createJobCmd = &cobra.Command{
		Use:   "inject-job [job type]",
		Short: "inject a new job into the queue",
		PreRun: func(cmd *cobra.Command, args []string) {
			if len(args) > 0 {
				jobType = args[0]
			} else {
				_ = cmd.Help()
				log.Fatal("must specify job type")
			}
		},
		Run: func(cmd *cobra.Command, _ []string) {
			var jobData json.RawMessage
			err := json.NewDecoder(os.Stdin).Decode(&jobData)
			if err != nil {
				log.Fatal("error parsing json input: ", err)
			}

			log.Info("injecting job ", jobType, " with data: ", bytes.NewBuffer(jobData).String())
			// TODO: Validate that jobData can be unmarshalled as jobType before injecting

			dbConn, err := sql.Open("postgres", config.C.DB)
			if err != nil {
				log.Fatal("error connecting to database:", err)
			}
			defer dbConn.Close()

			queries := db.New(dbConn)
			err = queries.CreateJob(cmd.Context(), db.CreateJobParams{
				JobID:          utils.GenerateID(),
				AssignedWorker: worker,
				State:          0,
				StateText:      "manually queued",
				JobType:        jobType,
				JobData:        jobData,
			})
			if err != nil {
				log.Fatal("error creating job: ", err)
			}
			log.Info("job created")
		},
	}
)

func init() {
	createJobCmd.Flags().StringVarP(&worker, "worker", "w", "", "The worker to assign this job to")
	DebugCmd.AddCommand(createJobCmd)
}
