// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package keycloak

import (
	jwt "github.com/dgrijalva/jwt-go"
)

type Claims struct {
	jwt.StandardClaims
	PreferredUsername string                              `json:"preferred_username"`
	Aud               []string                            `json:"aud"`
	ResourceAccess    map[string]struct{ Roles []string } `json:"resource_access"`
}
