#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only

set -exuo pipefail
echo "drop database rhyzome; create database rhyzome; grant all privileges on database rhyzome to $(whoami)" | sudo -u postgres psql
psql rhyzome < sql/schema.sql