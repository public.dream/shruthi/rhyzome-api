module git.callpipe.com/entanglement.garden/rhyzome-api

go 1.15

require (
	git.callpipe.com/entanglement.garden/rhyzome-openapi v0.0.0-20220502192255-b41d9991a224
	git.callpipe.com/entanglement.garden/rhyzome-protos v0.0.0-20211027040613-7a88aa3a3c38
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/labstack/echo/v4 v4.7.2
	github.com/lib/pq v1.10.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	google.golang.org/grpc v1.40.0
	google.golang.org/grpc/security/advancedtls v0.0.0-20210830204946-ef66d13abb84
)
