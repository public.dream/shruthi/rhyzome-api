-- Copyright 2021 Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only


-- TENANTS
-- name: CreateTenant :exec
INSERT INTO tenants (name, state) VALUES ($1, 0);

-- name: ListTenants :many
SELECT * FROM tenants;


-- JOBS
-- name: CreateJob :exec
INSERT INTO jobs (job_id, assigned_worker, state, state_text, job_type, job_data) VALUES ($1, $2, $3, $4, $5, $6);

-- name: GetNextJob :one
SELECT * FROM jobs WHERE state = 0;

-- name: GetNextJobForWorker :one
SELECT * FROM jobs WHERE state = 0 AND assigned_worker = $1;

-- name: SetJobState :execrows
UPDATE jobs SET state = $1, state_text = $2 WHERE job_id = $3;

-- name: GetFailedJobs :many
SELECT * FROM jobs WHERE state = 3;

-- name: DeleteJob :exec
DELETE FROM jobs WHERE job_id = $1;

-- name: GetJob :one
SELECT * FROM jobs WHERE job_id = $1;


-- RESOURCES
-- name: CreateResource :exec
INSERT INTO resources (resource_id, tenant) VALUES ($1, $2);

-- name: DeleteResource :exec
DELETE FROM resources WHERE resource_id = $1;


-- INSTANCES
-- name: CreateInstance :exec
INSERT INTO instances (resource_id, base_image, memory, cpu, user_data, root_volume) VALUES ($1, $2, $3, $4, $5, $6);

-- name: SetInstanceState :exec
UPDATE instances SET state = $1 WHERE resource_id = $2;

-- name: GetInstance :one
SELECT * FROM instances WHERE resource_id = $1;

-- name: GetInstanceAttachedVolumes :many
SELECT * FROM volumes WHERE instance = $1;

-- name: ListInstances :many
SELECT instances.* FROM resources, instances WHERE resources.tenant = $1 AND resources.resource_id = instances.resource_id;

-- name: AttachVolumeToInstance :execrows
UPDATE volumes SET instance = $1 WHERE resource_id = $2;

-- name: DetachVolume :execrows
UPDATE volumes SET instance = NULL WHERE resource_id = $1;

-- name: DeleteInstance :exec
DELETE FROM instances WHERE resource_id = $1;


-- VOLUMES
-- name: CreateVolume :exec
INSERT INTO volumes (resource_id, node, size) VALUES ($1, $2, $3);

-- name: ListVolumes :many
SELECT volumes.* FROM resources, volumes WHERE resources.tenant = $1 AND resources.resource_id = volumes.resource_id;

-- name: DeleteVolume :exec
DELETE FROM volumes WHERE resource_id = $1;


-- NODES
-- name: GetNodes :many
SELECT * FROM nodes;

-- name: GetAvailableNodes :many
SELECT resource_id FROM nodes WHERE available = true;

-- name: GetNodeByHostname :one
SELECT * FROM nodes WHERE hostname = $1;

-- name: AddNode :one
INSERT INTO nodes (resource_id, hostname, available) VALUES ($1, $2, $3) RETURNING *;

-- name: SetNodeHostname :execrows
UPDATE nodes SET hostname = $1 WHERE resource_id = $2;

-- name: SetNodeAvailability :execrows
UPDATE nodes SET available = $1 WHERE hostname = $2;

-- name: DeleteNode :execrows
DELETE FROM nodes WHERE resource_id = $1;

-- name: GetNodeForInstance :one
SELECT nodes.* FROM nodes, instances WHERE nodes.resource_id = instances.node AND instances.resource_id = $1;

-- name: SetNodeForInstance :execrows
UPDATE instances SET node = $1 WHERE resource_id = $2;


-- OPENWRTS
-- name: GetOpenwrtByID :one
SELECT * FROM openwrts WHERE openwrt_id = $1;

-- name: GetAllOpenwrtIDs :many
SELECT openwrt_id FROM openwrts;

-- name: AddOpenwrt :one
INSERT INTO openwrts (openwrt_id) VALUES ($1) RETURNING *;


-- NETWORKS
-- name: CreateNetwork :exec
INSERT INTO networks (resource_id, tenant, vlan_id) VALUES ($1, $2, $3);

-- name: DeleteNetwork :exec
DELETE FROM networks WHERE resource_id = $1;

-- name: GetNetwork :one
SELECT * FROM networks WHERE resource_id = $1;

-- name: ListNetworks :many
SELECT resource_id FROM networks;