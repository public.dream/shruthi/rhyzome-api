GO_FILES = $(shell find ./ -type f -name '*.go')

rhyzome-api: $(GO_FILES) db/db.go
	go build ./cmd/rhyzome-api

db/db.go db/models.go db/queries.go: sql/schema.sql sql/queries.sql sqlc.yaml
	sqlc generate
