// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httputil

import (
	"context"
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type Error struct {
	Message string
	Error   string
}

func Err(ctx context.Context, w http.ResponseWriter, e error, message string, code int) {
	log.WithFields(log.Fields{"message": message, "error": e, "code": code}).Info("sending error response")
	w.WriteHeader(code)
	err := json.NewEncoder(w).Encode(Error{Message: message, Error: e.Error()})
	if err != nil {
		log.Error("error encoding error response: ", err)
	}
}
