// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpapi

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"math/big"
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/keycloak"
)

var (
	rsakeys       map[string]*rsa.PublicKey
	jwtContextKey = "rhyzome-jwt"
)

func GetPublicKeys() {
	rsakeys = make(map[string]*rsa.PublicKey)
	var body map[string]interface{}
	resp, _ := http.Get(config.C.Keycloak.JWKS)
	_ = json.NewDecoder(resp.Body).Decode(&body)
	for _, bodykey := range body["keys"].([]interface{}) {
		key := bodykey.(map[string]interface{})
		kid := key["kid"].(string)
		rsakey := new(rsa.PublicKey)
		number, _ := base64.RawURLEncoding.DecodeString(key["n"].(string))
		rsakey.N = new(big.Int).SetBytes(number)
		rsakey.E = 65537
		rsakeys[kid] = rsakey
	}
}

func keyfunc(token *jwt.Token) (interface{}, error) {
	return rsakeys[token.Header["kid"].(string)], nil
}

func authnMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	GetPublicKeys()
	return func(c echo.Context) error {
		if strings.HasPrefix(c.Request().URL.Path, "/.well-known") {
			return next(c)
		}

		isValid := false
		tokenString := c.Request().Header.Get("Authorization")
		if strings.HasPrefix(tokenString, "Bearer ") {
			tokenString = strings.TrimPrefix(tokenString, "Bearer ")
			token, err := jwt.ParseWithClaims(tokenString, &keycloak.Claims{}, keyfunc)
			if err != nil {
				log.Warn("error parsing JWT: ", err)
			} else if !token.Valid {
				err = errors.New("Invalid token")
			} else if token.Header["alg"] == nil {
				err = errors.New("alg must be defined")
				// } else if token.Claims.(jwt.MapClaims)["aud"] != "api://default" {
				// 	errorMessage = fmt.Sprintf("Invalid aud: %s", token.Claims.(jwt.MapClaims)["aud"])
				// } else if !strings.Contains(token.Claims.(jwt.MapClaims)["iss"].(string), os.Getenv("OKTA_DOMAIN")) {
				// 	errorMessage = "Invalid iss"
			} else {
				isValid = true
				c.Set(jwtContextKey, token)
			}
			if !isValid {
				log.Warn("rejecting request (forbidden): ", err)
				return c.JSON(http.StatusForbidden, err)
			}
		} else {
			log.Warn("rejecting request (unauthorized)")
			return c.JSON(http.StatusUnauthorized, map[string]string{"error": "no request token"})
		}

		return next(c)
	}
}
