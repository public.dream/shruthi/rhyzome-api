// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpapi

import (
	"net/http"
	"strings"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/keycloak"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

var (
	permissionsTable = map[string]map[string][]string{
		"/v0/instance": {
			"GET":  {"instance-list"},
			"POST": {"instance-create"},
		},
		"/v0/instance/:id": {
			"GET":    {"instance-get"},
			"DELETE": {"instance-delete"},
		},
	}
)

func authzMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		logger := log.WithFields(log.Fields{"path": c.Path(), "method": c.Request().Method})

		if strings.HasPrefix(c.Request().URL.Path, "/.well-known") {
			return next(c)
		}

		token, ok := c.Get(jwtContextKey).(*jwt.Token)
		if !ok || token == nil {
			logger.Debug("not running authz for request with no token on context")
			return next(c)
		}

		if config.C.DisableAuthz {
			logger.Debug("authz disabled, not checking permissions")
			return next(c)
		}

		claims := token.Claims.(*keycloak.Claims)

		// logger.Debug("permissions: ", claims.ResourceAccess[config.C.Keycloak.ResourceScope])

		requiredPermissions, ok := permissionsTable[c.Path()][c.Request().Method]
		if !ok {
			logger.Warn("no permission scopes for path/method")
			return next(c)
		}

		for _, permission := range claims.ResourceAccess[config.C.Keycloak.ResourceScope].Roles {
			for _, requiredPermission := range requiredPermissions {
				if requiredPermission == permission {
					logger.Debug("permission match: ", permission)
					return next(c)
				}
			}
		}

		logger.Debug("rejecting request due to lack of permissions")
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"error":                "user unauthorized for this request",
			"user":                 claims.PreferredUsername,
			"permissions":          claims.ResourceAccess[config.C.Keycloak.ResourceScope].Roles,
			"required_permissions": requiredPermissions,
		})
	}
}
