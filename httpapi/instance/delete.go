// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instance

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	grpcserver_libvirt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/libvirt"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (InstanceServer) DeleteInstance(ctx echo.Context, id string) error {
	log.Info("creating instance")

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	node, err := queries.GetNodeForInstance(ctx.Request().Context(), id)
	if err != nil {
		log.Fatal("error finding instance: ", err)
		return err
	}

	jobData, err := json.Marshal(grpcserver_libvirt.DeleteRequest{Id: id})
	if err != nil {
		log.Fatal("error marshaling delete job: ", err)
		return err
	}

	jobID := utils.GenerateID()
	err = queries.CreateJob(ctx.Request().Context(), db.CreateJobParams{
		JobID:          jobID,
		AssignedWorker: node.ResourceID,
		StateText:      "QUEUED",
		JobType:        grpcserver_libvirt.DeleteInstanceJobTypeV1,
		JobData:        jobData,
	})
	if err != nil {
		log.Error("error scheduling job: ", err)
		return err
	}

	log.Debug("job created")
	return ctx.JSON(http.StatusOK, rhyzome.Job{JobId: jobID})
}
