// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instance

import (
	"database/sql"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (InstanceServer) ListAllInstances(ctx echo.Context) error {
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Error("error connecting to database: ", err)
		return err
	}
	defer dbConn.Close()

	queries := db.New(dbConn)
	instances, err := queries.ListInstances(ctx.Request().Context(), db.DefaultTenantName)
	if err != nil {
		log.Error("error listing instances: ", err)
		return err
	}

	response := []rhyzome.Instance{}
	for _, instance := range instances {
		response = append(response, rhyzome.Instance{
			Cpu:        int(instance.Cpu),
			Memory:     int(instance.Memory),
			Node:       instance.Node.String,
			ResourceId: instance.ResourceID,
			RootVolume: instance.RootVolume,
			State:      states[instance.State],
		})
	}
	return ctx.JSON(200, response)
}
