// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instance

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

var states = map[int32]rhyzome.InstanceState{
	db.InstanceStatePENDING:  rhyzome.InstanceStatePENDING,
	db.InstanceStateCREATING: rhyzome.InstanceStateCREATING,
	db.InstanceStateSTARTED:  rhyzome.InstanceStateRUNNING,
	db.InstanceStateSTOPPED:  rhyzome.InstanceStateSTOPPED,
	db.InstanceStateDELETING: rhyzome.InstanceStateDELETING,
	db.InstanceStateDELETED:  rhyzome.InstanceStateDELETED,
	db.InstanceStateERROR:    rhyzome.InstanceStateERROR,
}

func (InstanceServer) GetInstance(ctx echo.Context, id string) error {
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Error("error connecting to database: ", err)
		return err
	}
	defer dbConn.Close()

	queries := db.New(dbConn)
	instance, err := queries.GetInstance(ctx.Request().Context(), id)
	if err != nil {
		log.Error("error getting instance state: ", err)
		return err
	}

	userData := string(instance.UserData)
	return ctx.JSON(http.StatusOK, rhyzome.Instance{
		Cpu:        int(instance.Cpu),
		Memory:     int(instance.Memory),
		Node:       instance.Node.String,
		ResourceId: instance.ResourceID,
		RootVolume: instance.RootVolume,
		UserData:   &userData,
		State:      states[instance.State],
	})
}
