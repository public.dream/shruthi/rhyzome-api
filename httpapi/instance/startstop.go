// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instance

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	grpcserver_libvirt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/libvirt"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (InstanceServer) StartInstance(ctx echo.Context, instanceID string) error {
	logger := log.WithField("instance", instanceID)
	logger.Info("processing start instance")

	logger.Debug("connecting to database")
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		logger.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	logger.Debug("querying DB for node to connect to")
	node, err := queries.GetNodeForInstance(ctx.Request().Context(), instanceID)
	if err != nil {
		logger.Error("error selecting node: ", err)
		return err
	}

	logger = logger.WithField("node", node.Hostname)

	jobData, err := json.Marshal(grpcserver_libvirt.StartStopRequest{ResourceID: instanceID})
	if err != nil {
		logger.Error("error marshaling job: ", err)
		return err
	}

	jobID := utils.GenerateID()
	err = queries.CreateJob(ctx.Request().Context(), db.CreateJobParams{
		JobID:          jobID,
		AssignedWorker: node.ResourceID,
		StateText:      "QUEUED",
		JobType:        grpcserver_libvirt.StartInstanceJobTypeV1,
		JobData:        jobData,
	})
	if err != nil {
		logger.Error("error scheduling job: ", err)
		return err
	}
	logger.Debug("instance start requested")

	return ctx.JSON(http.StatusCreated, rhyzome.Job{JobId: jobID})
}

func (InstanceServer) StopInstance(ctx echo.Context, instanceID string) error {
	logger := log.WithField("instance", instanceID)
	logger.Info("processing stop instance")

	logger.Debug("connecting to database")
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		logger.Error("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	logger.Debug("querying DB for node to connect to")
	node, err := queries.GetNodeForInstance(ctx.Request().Context(), instanceID)
	if err != nil {
		logger.Error("error selecting node: ", err)
		return err
	}

	logger = logger.WithField("node", node.Hostname)

	jobData, err := json.Marshal(grpcserver_libvirt.StartStopRequest{ResourceID: instanceID})
	if err != nil {
		logger.Error("error marshaling job: ", err)
		return err
	}

	jobID := utils.GenerateID()
	err = queries.CreateJob(ctx.Request().Context(), db.CreateJobParams{
		JobID:          jobID,
		AssignedWorker: node.ResourceID,
		StateText:      "QUEUED",
		JobType:        grpcserver_libvirt.StartInstanceJobTypeV1,
		JobData:        jobData,
	})
	if err != nil {
		logger.Error("error scheduling job: ", err)
		return err
	}
	logger.Debug("instance start requested")

	return ctx.JSON(http.StatusCreated, rhyzome.Job{JobId: jobID})
}
