// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instance

import (
	"database/sql"
	"encoding/json"
	"math/rand"
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	grpcserver_libvirt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/libvirt"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (InstanceServer) CreateInstance(c echo.Context) error {
	log.Info("creating instance")
	ctx := c.Request().Context()

	var req rhyzome.CreateInstanceRequestBody
	if err := c.Bind(&req); err != nil {
		return err
	}

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	resourceID := utils.GenerateID()
	jobID := utils.GenerateID()
	volumeID := utils.GenerateID()

	err = queries.CreateResource(ctx, db.CreateResourceParams{ResourceID: resourceID, Tenant: db.DefaultTenantName})
	if err != nil {
		log.Error("error creating resource "+resourceID+": ", err)
		return err
	}

	err = queries.CreateResource(ctx, db.CreateResourceParams{ResourceID: volumeID, Tenant: db.DefaultTenantName})
	if err != nil {
		log.Error("error creating resource "+resourceID+": ", err)
		return err
	}

	userData := []byte{}
	if req.UserData != nil {
		userData = []byte(*req.UserData)
	}

	err = queries.CreateInstance(ctx, db.CreateInstanceParams{
		ResourceID: resourceID,
		BaseImage:  req.BaseImage,
		Memory:     int32(req.Memory),
		Cpu:        int32(req.Cpu),
		UserData:   userData,
		RootVolume: volumeID,
	})
	if err != nil {
		log.Error("error creating instance: ", err)
		return err
	}

	log.Debug("added instance to DB")

	jobData, err := json.Marshal(grpcserver_libvirt.CreateRequest{
		Id:           resourceID,
		UserData:     userData,
		Memory:       req.Memory,
		Cores:        req.Cpu,
		BaseImage:    req.BaseImage,
		VolumeSize:   req.DiskSize,
		Tenant:       db.DefaultTenantName,
		RootVolumeID: volumeID,
	})
	if err != nil {
		log.Error("error creating job: ", err)
		return err
	}

	nodes, err := queries.GetAvailableNodes(ctx)
	if err != nil {
		log.Error("error assigning node: ", err)
		return err
	}

	nodeCount := len(nodes)
	if nodeCount < 1 {
		log.Error("no nodes available for new instance")
		return err
	}

	node := nodes[rand.Intn(nodeCount)]
	log.Debug("assigned to node ", node)

	err = queries.CreateJob(ctx, db.CreateJobParams{
		JobID:          jobID,
		AssignedWorker: node,
		StateText:      "QUEUED",
		JobType:        grpcserver_libvirt.CreateInstanceJobTypeV1,
		JobData:        jobData,
	})
	if err != nil {
		log.Error("error scheduling job: ", err)
		return err
	}
	log.Debug("job created")
	return c.JSON(http.StatusCreated, rhyzome.CreateInstanceResponseBody{InstanceId: resourceID, JobId: jobID})
}
