// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package network

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	grpcserver_openwrt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/openwrt"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (NetworkServer) DeleteNetwork(ctx echo.Context, id string) error {
	log.Info("deleting network")

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	jobData, err := json.Marshal(grpcserver_openwrt.DeleteRequest{ID: id})
	if err != nil {
		log.Fatal("error marshaling delete job: ", err)
		return err
	}

	openwrtIDs, err := queries.GetAllOpenwrtIDs(ctx.Request().Context())
	if err != nil {
		log.Error("failed to query all openwrt IDs")
		return err
	}

	jobID := utils.GenerateID()

	for _, openwrtID := range openwrtIDs {
		err = queries.CreateJob(ctx.Request().Context(), db.CreateJobParams{
			JobID:          jobID,
			AssignedWorker: openwrtID,
			StateText:      "QUEUED",
			JobType:        grpcserver_openwrt.DeleteNetworkJobTypeV1,
			JobData:        jobData,
		})
		if err != nil {
			log.Error("error scheduling job: ", err)
			return err
		}
		log.Debug("job created")
	}

	log.Debug("job created")
	return ctx.JSON(http.StatusOK, rhyzome.Job{JobId: jobID})
}
