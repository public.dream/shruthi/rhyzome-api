// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package network

import (
	"database/sql"
	"encoding/json"
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	grpcserver_openwrt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/openwrt"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (NetworkServer) CreateNetwork(c echo.Context) error {
	log.Info("creating network")
	ctx := c.Request().Context()

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	resourceID := utils.GenerateID()
	jobID := utils.GenerateID()

	err = queries.CreateResource(ctx, db.CreateResourceParams{ResourceID: resourceID, Tenant: db.DefaultTenantName})
	if err != nil {
		log.Error("error creating resource "+resourceID+": ", err)
		return err
	}

	vlanID := 10

	err = queries.CreateNetwork(ctx, db.CreateNetworkParams{
		ResourceID: resourceID,
		Tenant:     db.DefaultTenantName,
		VlanID:     int32(vlanID),
	})
	if err != nil {
		log.Error("error creating network: ", err)
		return err
	}

	log.Debug("added network to DB")

	jobData, err := json.Marshal(grpcserver_openwrt.CreateRequest{
		ID:     resourceID,
		Tenant: db.DefaultTenantName,
		VlanID: vlanID,
	})
	if err != nil {
		log.Error("error creating job: ", err)
		return err
	}

	openwrtIDs, err := queries.GetAllOpenwrtIDs(ctx)
	if err != nil {
		log.Error("failed to query all openwrt IDs")
		return err
	}

	for _, openwrtID := range openwrtIDs {
		err = queries.CreateJob(ctx, db.CreateJobParams{
			JobID:          jobID,
			AssignedWorker: openwrtID,
			StateText:      "QUEUED",
			JobType:        grpcserver_openwrt.CreateNetworkJobTypeV1,
			JobData:        jobData,
		})
		if err != nil {
			log.Error("error scheduling job: ", err)
			return err
		}
		log.Debug("job created")
	}

	return c.JSON(http.StatusCreated, rhyzome.CreateInstanceResponseBody{InstanceId: resourceID, JobId: jobID})
}
