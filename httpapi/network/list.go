// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package network

import (
	"database/sql"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func (NetworkServer) ListAllNetworks(ctx echo.Context) error {
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Error("error connecting to database: ", err)
		return err
	}
	defer dbConn.Close()

	queries := db.New(dbConn)
	networks, err := queries.ListNetworks(ctx.Request().Context())
	if err != nil {
		log.Error("error listing networks: ", err)
		return err
	}

	response := []rhyzome.Network{}
	for _, network := range networks {
		response = append(response, rhyzome.Network{
			Id: network,
		})
	}
	return ctx.JSON(200, response)
}
