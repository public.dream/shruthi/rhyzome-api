// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package jobs

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

var states = map[int32]rhyzome.JobState{
	db.JobStateQUEUED:  rhyzome.JobStateQUEUED,
	db.JobStateRUNNING: rhyzome.JobStateRUNNING,
	db.JobStateDONE:    rhyzome.JobStateDONE,
	db.JobStateFAILED:  rhyzome.JobStateFAILED,
}

func (JobServer) GetJob(ctx echo.Context, id string) error {
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Error("error connecting to database: ", err)
		return err
	}
	defer dbConn.Close()

	queries := db.New(dbConn)
	job, err := queries.GetJob(ctx.Request().Context(), id)
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, rhyzome.Job{
		JobId:     id,
		JobType:   job.JobType,
		State:     states[job.State],
		StateText: job.StateText,
		CreatedAt: job.CreatedAt.Unix(),
	})
}
