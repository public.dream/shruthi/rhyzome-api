// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package jobs

import (
	"errors"

	"github.com/labstack/echo/v4"
)

type JobServer struct{}

func (JobServer) StreamJob(ctx echo.Context, _ string) error {
	return errors.New("not implemented")
}
