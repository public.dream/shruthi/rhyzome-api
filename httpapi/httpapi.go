// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpapi

import (
	"context"
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/httpapi/instance"
	"git.callpipe.com/entanglement.garden/rhyzome-api/httpapi/jobs"
	"git.callpipe.com/entanglement.garden/rhyzome-api/httpapi/network"
	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

var e *echo.Echo

type server struct {
	instance.InstanceServer
	network.NetworkServer
	jobs.JobServer
}

func Serve() {
	e = echo.New()
	e.HideBanner = true
	e.HidePort = true
	log.Println("starting http listener on", config.C.HTTPBind)
	var rhyzomeServer server
	rhyzome.RegisterHandlers(e, &rhyzomeServer)
	e.Use(loggerMiddleware)
	// e.Use(authnMiddleware)
	// e.Use(authzMiddleware)

	err := e.Start(config.C.HTTPBind)
	if err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func Shutdown(ctx context.Context) {
	if e != nil {
		log.Info("shutting down http listener")
		err := e.Shutdown(ctx)
		if err != nil {
			log.Error("error shutting down http server: ", err)
		}
	}
}

func loggerMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := next(c)
		logger := log.WithFields(log.Fields{
			"method": c.Request().Method,
			"path":   c.Request().URL.Path,
		})
		if err != nil {
			logger.WithField("error", err)
		}
		logger.Info("request handled")
		return err
	}
}

func (server) ProvisioningInfo(ctx echo.Context) error {
	return ctx.JSON(200, config.C.ProvisioningInfo)
}

func (server) ProvisioningCaPem(ctx echo.Context) error {
	return ctx.File(config.C.PKI.CA)
}
