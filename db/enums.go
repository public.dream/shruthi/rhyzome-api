// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

const (
	DefaultTenantName = "default" // this is temporary until tenant management exists

	// tenants.state
	TenantStatePENDING      = 0
	TenantStatePROVISIONING = 1
	TenantStateACTIVE       = 2
	TenantStateSUSPENDED    = 3
	TenantStateDELETED      = 4

	// jobs.state
	JobStateQUEUED  = 0
	JobStateRUNNING = 1
	JobStateDONE    = 2
	JobStateFAILED  = 3

	// instances.state
	InstanceStatePENDING  = 0
	InstanceStateCREATING = 1
	InstanceStateSTARTED  = 2
	InstanceStateSTOPPED  = 3
	InstanceStateDELETING = 4
	InstanceStateDELETED  = 5
	InstanceStateERROR    = 6
)
