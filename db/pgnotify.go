// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import "encoding/json"

type PGNotifyMessage struct {
	Table  string
	Action string
	Data   struct {
		JobID          string          `json:"job_id"`
		AssignedWorker string          `json:"assigned_worker"`
		State          int             `json:"state"`
		StateText      string          `json:"state_text"`
		JobType        string          `json:"job_type"`
		JobData        json.RawMessage `json:"job_data"`
	}
}
