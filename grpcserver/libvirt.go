// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"

	grpcserver_libvirt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/libvirt"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type libvirtServer struct {
	rhyzome_protos.UnimplementedRhyzomeLibvirtServer
}

func (libvirtServer) EventStream(events rhyzome_protos.RhyzomeLibvirt_EventStreamServer) error {
	return grpcserver_libvirt.EventStream(events)
}

func (libvirtServer) InstanceStarted(ctx context.Context, req *rhyzome_protos.InstanceStartedRequest) (*rhyzome_protos.InstanceStartedResponse, error) {
	return grpcserver_libvirt.InstanceStarted(ctx, req)
}

func (libvirtServer) InstanceStopped(ctx context.Context, req *rhyzome_protos.InstanceStoppedRequest) (*rhyzome_protos.InstanceStoppedResponse, error) {
	return grpcserver_libvirt.InstanceStopped(ctx, req)
}
