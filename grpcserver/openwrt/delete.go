// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_openwrt

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type DeleteRequest struct {
	ID string
}

func DeleteNetwork(ctx context.Context, request json.RawMessage) error {
	// parse request
	var req DeleteRequest
	if err := json.Unmarshal(request, &req); err != nil {
		log.Error("error parsing delete network job: ", bytes.NewBuffer(request).String())
		return err
	}

	openwrt := getOpenwrtFromContext(ctx)

	log.Info("deleting network")
	resp, err := SendEvent(openwrt.OpenwrtID, &rhyzome_protos.OpenwrtEvent{
		DeleteNetwork: &rhyzome_protos.DeleteNetwork{ResourceId: req.ID},
	})
	if err != nil {
		return err
	}

	if resp.Type == rhyzome_protos.OpenwrtEventUpdate_FAILED {
		log.Warn("failed to delete network: ", resp.Message)
		return errors.New(resp.Message)
	}
	log.Debug("network deleted")

	return nil
}
