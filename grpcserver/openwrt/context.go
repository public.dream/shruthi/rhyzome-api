// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_openwrt

import (
	"context"

	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

type contextKey int

var (
	contextKeyOpenwrt = contextKey(1)
	contextKeyTenant  = contextKey(2)
)

func getOpenwrtFromContext(ctx context.Context) *db.Openwrt {
	return ctx.Value(contextKeyOpenwrt).(*db.Openwrt)
}

// func getTenantFromContext(ctx context.Context) string {
// 	return ctx.Value(contextKeyTenant).(string)
// }
