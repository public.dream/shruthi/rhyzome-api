// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_openwrt

import (
	"context"
	"database/sql"
	"errors"
	"io"
	"sync"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/peer"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

var (
	clients          = make(map[string]*rhyzome_protos.RhyzomeOpenwrt_EventStreamServer)
	clientsMux       = sync.Mutex{}
	eventsChannel    = make(map[string]chan *rhyzome_protos.OpenwrtEventUpdate)
	eventsChannelMux = sync.Mutex{}
)

func EventStream(events rhyzome_protos.RhyzomeOpenwrt_EventStreamServer) error {
	ctx := events.Context()
	p, ok := peer.FromContext(ctx)
	if !ok {
		log.Error("grpc method EventStream requested by client with no tls peer")
		return errors.New("tls is mandatory")
	}

	tlsInfo, ok := p.AuthInfo.(credentials.TLSInfo)
	if !ok {
		log.Error("failed to get tls credentials")
		return errors.New("tls is mandatory")
	}

	log.Debug("grpc client connected")

	for _, cert := range tlsInfo.State.PeerCertificates {
		log.Info("rpc connection from tls peer: ", cert.Subject, cert.DNSNames)
	}

	if len(tlsInfo.State.VerifiedChains) == 0 || len(tlsInfo.State.VerifiedChains[0]) == 0 {
		log.Warn("client certificate not verified")
		// reducing this to a warning until we can figure out why it's not populating.
		// return errors.New("certificate not verified")
	}

	for _, chains := range tlsInfo.State.VerifiedChains {
		for _, chain := range chains {
			log.Info("verified tls chain: ", chain.Subject)
		}
	}

	if len(tlsInfo.State.PeerCertificates) == 0 {
		log.Error("no peer certificate presented")
		return errors.New("no peer cert presented")
	}

	openwrtID := tlsInfo.State.PeerCertificates[0].Subject.CommonName
	logger := log.WithField("node", openwrtID)
	logger.Info("grpc connection from ", openwrtID)

	clientsMux.Lock()
	_, ok = clients[openwrtID]
	if ok {
		logger.Error("refusing duplicate connection from node ", openwrtID)
		clientsMux.Unlock()
		return errors.New("duplicate connection from node not allowed")
	}
	clients[openwrtID] = &events
	clientsMux.Unlock()

	defer func() {
		clientsMux.Lock()
		_, ok := clients[openwrtID]
		if !ok {
			logger.Debug("EventStream endpoint closed with no clients for this nodename")
			clientsMux.Unlock()
			return
		}
		delete(clients, openwrtID)
		clientsMux.Unlock()
	}()

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		logger.Error("error connecting to database: ", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	err = ensureOpenwrt(ctx, queries, openwrtID)
	if err != nil {
		logger.Error("error getting Openwrt Server from db: ", err)
		return err
	}

	ctx = context.WithValue(ctx, contextKeyOpenwrt, &db.Openwrt{OpenwrtID: openwrtID})
	go listen(ctx)
	for {
		update, err := events.Recv()
		if err != nil {
			if err == io.EOF {
				logger.Debug("got eof")
				return nil
			}
			if err == context.Canceled {
				logger.Debug("context canceled, stopping listener")
				return nil
			}
			logger.Warn("error receiving event from grpc client: ", err)
			// continue
			return err // until we can correctly detect context cancelled
		}
		eventLogger := logger.WithField("event_id", update.EventId)
		eventLogger.Debugf("received update from grpc client: %#v", update)
		eventsChannelMux.Lock()
		ch, ok := eventsChannel[update.EventId]
		if !ok {
			eventLogger.Debug("ignoring update for event ID with no listener")
			eventsChannelMux.Unlock()
			continue
		}
		ch <- update
		delete(eventsChannel, update.EventId)
		eventsChannelMux.Unlock()
		eventLogger.Debug("event delivered")
	}
}

func SendEvent(node string, event *rhyzome_protos.OpenwrtEvent) (*rhyzome_protos.OpenwrtEventUpdate, error) {
	if event.EventId == "" {
		event.EventId = uuid.New().String()
	}

	clientsMux.Lock()
	c, ok := clients[node]
	if !ok {
		clientsMux.Unlock()
		return nil, errors.New("node not connected")
	}

	ch := make(chan *rhyzome_protos.OpenwrtEventUpdate)
	eventsChannelMux.Lock()
	// hope we dont get duplicate event IDs
	eventsChannel[event.EventId] = ch
	eventsChannelMux.Unlock()

	log.WithField("event_id", event.EventId).Debug("sending event to ", node)
	err := (*c).Send(event)
	if err != nil {
		clientsMux.Unlock()
		return nil, err
	}
	clientsMux.Unlock()

	return <-ch, nil
}

func ensureOpenwrt(ctx context.Context, queries *db.Queries, id string) error {
	_, err := queries.GetOpenwrtByID(ctx, id)
	if err == sql.ErrNoRows {
		err = addOpenwrt(ctx, queries, id)
	}
	if err != nil {
		return err
	}
	return nil
}

func addOpenwrt(ctx context.Context, queries *db.Queries, id string) error {
	logger := log.WithFields(log.Fields{"id": id})
	logger.Info("registering new Openwrt")

	_, err := queries.AddOpenwrt(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
