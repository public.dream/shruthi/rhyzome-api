// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_openwrt

import (
	"bytes"
	"context"
	"encoding/json"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type CreateRequest struct {
	ID     string
	Tenant string

	// settings for the network
	VlanID int
}

func CreateNetwork(ctx context.Context, request json.RawMessage) error {
	// parse request
	var req CreateRequest
	if err := json.Unmarshal(request, &req); err != nil {
		log.Error("error parsing create network job: ", bytes.NewBuffer(request).String())
		return err
	}

	ctx = context.WithValue(ctx, contextKeyTenant, req.Tenant)

	openwrt := getOpenwrtFromContext(ctx)

	log.Info("creating network")
	_, err := SendEvent(openwrt.OpenwrtID, &rhyzome_protos.OpenwrtEvent{
		CreateNetwork: &rhyzome_protos.CreateNetwork{ResourceId: req.ID, VlanId: int32(req.VlanID)},
	})
	if err != nil {
		return err
	}
	log.Debug("network created")

	return nil
}
