// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_libvirt

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

const (
	// each job has a "type" string. Always reference this variable, dont copy the string.
	// they are versioned so we can have new versions not interfere. Maybe if we move to
	// protobuf for job state encoding that wouldnt be as important.
	CreateInstanceJobTypeV1 = "instance/create/v1"
	StartInstanceJobTypeV1  = "instance/start/v1"
	StopInstanceJobTypeV1   = "instance/stop/v1"
	DeleteInstanceJobTypeV1 = "instance/delete/v1"

	JobStatePENDING = 0
	JobStateRUNNING = 1
	JobStateDONE    = 2
	JobStateERROR   = 3
)

var (
	executors = map[string]func(context.Context, json.RawMessage) error{
		CreateInstanceJobTypeV1: CreateInstance,
		DeleteInstanceJobTypeV1: DeleteInstance,
		StartInstanceJobTypeV1:  StartInstance,
		StopInstanceJobTypeV1:   StopInstance,
	}
)

// listen runs as a go routine and listens for jobs assigned to the connected runner
func listen(ctx context.Context) {
	node := getNodeFromContext(ctx)
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal(err)
	}

	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			log.Fatal("error receiving from database event stream:", err)
		}
	}

	listener := pq.NewListener(config.C.DB, 10*time.Second, time.Minute, reportProblem)
	err = listener.Listen("jobs")
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	queries := db.New(dbConn)

	// iterate through backlog of queued jobs
	for {
		queuedJob, err := queries.GetNextJobForWorker(ctx, node.ResourceID)
		if err != nil {
			if err == sql.ErrNoRows {
				break
			}
			log.Error("error getting next job: ", err)
		}
		err = executeJob(ctx, queries, queuedJob.JobID, queuedJob.JobType, queuedJob.JobData)
		if err != nil {
			log.Error("error executing job: ", err)
		}
	}

	log.Debug("listening for updates from postgres")
	for {
		select {
		case n := <-listener.Notify:
			var message db.PGNotifyMessage
			if err := json.Unmarshal([]byte(n.Extra), &message); err != nil {
				log.Error("Error unmarshalling message from postgres: ", err)
			}
			log.Debugf("executing job: %+v", message.Data)
			if message.Action == "INSERT" {
				go func() {
					if err := executeJob(ctx, queries, message.Data.JobID, message.Data.JobType, message.Data.JobData); err == nil {
						log.Info("successfully executed job")
					} else {
						log.Error(err)
					}
				}()
			}
		case <-time.After(90 * time.Second):
			log.Trace("pinging database")
			go func() {
				err := listener.Ping()
				if err != nil {
					log.Fatal("error connecting to database:", err)
				} else {
					log.Trace("pong from database")
				}
			}()
		}
	}
}

func executeJob(ctx context.Context, queries *db.Queries, jobID string, jobType string, jobData json.RawMessage) error {
	execute, ok := executors[jobType]
	if !ok {
		return fmt.Errorf("unrecognized job type: %s", jobType)

	}
	logger := log.WithFields(log.Fields{"job_id": jobID, "job_type": jobType})
	logger.Info("Executing job ", jobType)
	if rows, err := queries.SetJobState(ctx, db.SetJobStateParams{State: JobStateRUNNING, StateText: "RUNNING", JobID: jobID}); err != nil {
		return fmt.Errorf("error updating job %s state: %w", jobType, err)
	} else {
		logger.Debug("marked job ", jobID, " as RUNNING, updated ", rows, " row(s)")
	}

	if err := execute(ctx, jobData); err != nil {
		if rows, dberr := queries.SetJobState(ctx, db.SetJobStateParams{State: JobStateERROR, StateText: "FAILED", JobID: jobID}); dberr != nil {
			logger.Error("error updating job ", jobID, " state: ", dberr)
		} else {
			logger.Debug("marked job ", jobID, " as FAILED, updated ", rows, " row(s)")
		}

		return fmt.Errorf("error executing job %s: %w", jobType, err)
	}
	if rows, err := queries.SetJobState(ctx, db.SetJobStateParams{State: JobStateDONE, StateText: "DONE", JobID: jobID}); err != nil {
		logger.Error("error updating job ", jobID, " state: ", err)
	} else {
		logger.Debug("marked job ", jobID, " as DONE, updated ", rows, " row(s)")
	}
	return nil
}
