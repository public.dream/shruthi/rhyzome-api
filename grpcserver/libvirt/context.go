// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_libvirt

import (
	"context"

	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
)

type contextKey int

var (
	contextKeyNode   = contextKey(1)
	contextKeyTenant = contextKey(2)
)

func getNodeFromContext(ctx context.Context) db.Node {
	return ctx.Value(contextKeyNode).(db.Node)
}

// func getTenantFromContext(ctx context.Context) string {
// 	return ctx.Value(contextKeyTenant).(string)
// }
