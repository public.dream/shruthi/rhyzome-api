// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_libvirt

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type StartStopRequest struct {
	ResourceID string
}

// StartInstance requests that libvirt boot a VM
func StartInstance(ctx context.Context, request json.RawMessage) error {
	var req StartStopRequest
	if err := json.Unmarshal(request, &req); err != nil {
		log.Error("error parsing create instance job: ", bytes.NewBuffer(request).String())
		return err
	}

	node := getNodeFromContext(ctx)

	log.Debug("starting instance")
	_, err := SendEvent(node.Hostname, &rhyzome_protos.LibvirtEvent{
		StartInstance: &rhyzome_protos.StartInstance{ResourceId: req.ResourceID},
	})
	if err != nil {
		log.Error("error starting instance: ", err)
		return err
	}
	log.Debug("instance started")
	return nil
}

// InstanceStarted callback is called when a VM starts up
func InstanceStarted(ctx context.Context, req *rhyzome_protos.InstanceStartedRequest) (*rhyzome_protos.InstanceStartedResponse, error) {
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Error("error connecting to database: ", err)
		return nil, err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{State: int32(db.InstanceStateSTARTED), ResourceID: req.Id})
	if err != nil {
		log.Error("error setting instance state: ", err)
		return nil, err
	}
	log.WithField("instance_id", req.Id).Info("instance started")

	return &rhyzome_protos.InstanceStartedResponse{}, nil
}

// StopInstance requests that libvirt boot a VM
func StopInstance(ctx context.Context, request json.RawMessage) error {
	var req StartStopRequest
	if err := json.Unmarshal(request, &req); err != nil {
		log.Error("error parsing create instance job: ", bytes.NewBuffer(request).String())
		return err
	}

	node := getNodeFromContext(ctx)

	log.Debug("stoppusing the default sudo prompt?ing instance")
	_, err := SendEvent(node.Hostname, &rhyzome_protos.LibvirtEvent{
		StopInstance: &rhyzome_protos.StopInstance{ResourceId: req.ResourceID},
	})
	if err != nil {
		log.Error("error stopping instance: ", err)
		return err
	}
	log.Debug("instance stopped")
	return nil
}

// InstanceStopped call the on vm stop hook
func InstanceStopped(ctx context.Context, req *rhyzome_protos.InstanceStoppedRequest) (*rhyzome_protos.InstanceStoppedResponse, error) {
	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Error("error connecting to database: ", err)
		return nil, err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	instance, err := queries.GetInstance(ctx, req.Id)
	if err != nil {
		log.Error("error finding instance: ", err)
		return nil, err
	}

	if instance.State == db.InstanceStateDELETED || instance.State == db.InstanceStateDELETING {
		return &rhyzome_protos.InstanceStoppedResponse{}, nil
	}

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{State: int32(db.InstanceStateSTOPPED), ResourceID: req.Id})
	if err != nil {
		log.Error("error setting instance state: ", err)
		return nil, err
	}
	log.WithField("instance_id", req.Id).Info("instance stopped")

	return &rhyzome_protos.InstanceStoppedResponse{}, nil
}
