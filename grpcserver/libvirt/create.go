// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_libvirt

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type CreateRequest struct {
	Id       string
	Tenant   string
	UserData []byte
	Memory   int
	Cores    int

	// settings for the root volume
	RootVolumeID      string
	BaseImage         string
	VolumeSize        int
	AdditionalVolumes []string
}

func CreateInstance(ctx context.Context, request json.RawMessage) error {
	// parse request
	var req CreateRequest
	if err := json.Unmarshal(request, &req); err != nil {
		log.Error("error parsing create instance job: ", bytes.NewBuffer(request).String())
		return err
	}

	ctx = context.WithValue(ctx, contextKeyTenant, req.Tenant)

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	node := getNodeFromContext(ctx)

	_, err = queries.SetNodeForInstance(ctx, db.SetNodeForInstanceParams{Node: sql.NullString{Valid: true, String: node.ResourceID}, ResourceID: req.Id})
	if err != nil {
		log.Error("error setting node in db: ", err)
		return err
	}

	log.WithFields(log.Fields{"resource_id": req.RootVolumeID, "node": node.ResourceID, "size": req.VolumeSize}).Debug("creating volume")
	err = queries.CreateVolume(ctx, db.CreateVolumeParams{
		ResourceID: req.RootVolumeID,
		Node:       node.ResourceID,
		Size:       int32(req.VolumeSize),
	})
	if err != nil {
		log.Error("error adding root volume to db: ", err)
		return err
	}

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{
		State:      int32(db.InstanceStateCREATING),
		ResourceID: req.Id,
	})
	if err != nil {
		log.Error("error updating instance state:", err)
		return err
	}

	log.Info("creating instance")
	_, err = SendEvent(node.Hostname, &rhyzome_protos.LibvirtEvent{
		CreateInstance: &rhyzome_protos.CreateInstance{
			ResourceId: req.Id,
			UserData:   req.UserData,
			RootVolume: &rhyzome_protos.CreateVolume{
				SourceImage: req.BaseImage,
				Size:        int32(req.VolumeSize),
				ResourceId:  req.RootVolumeID,
			},
			Memory:            int32(req.Memory),
			Cores:             int32(req.Cores),
			AdditionalVolumes: req.AdditionalVolumes,
		},
	})
	if err != nil {
		dberr := queries.SetInstanceState(ctx, db.SetInstanceStateParams{
			State:      int32(db.InstanceStateERROR),
			ResourceID: req.Id,
		})
		if dberr != nil {
			log.Error("error updating instance state:", err)
		}
		return err
	}
	log.Debug("instance created")

	return nil
}
