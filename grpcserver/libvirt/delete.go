// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_libvirt

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"errors"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type DeleteRequest struct {
	Id string
}

func DeleteInstance(ctx context.Context, request json.RawMessage) error {
	// parse request
	var req DeleteRequest
	if err := json.Unmarshal(request, &req); err != nil {
		log.Error("error parsing delete instance job: ", bytes.NewBuffer(request).String())
		return err
	}

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		log.Fatal("error connecting to database:", err)
		return err
	}
	defer dbConn.Close()
	queries := db.New(dbConn)

	node := getNodeFromContext(ctx)

	instance, err := queries.GetInstance(ctx, req.Id)
	if err != nil {
		log.Error("error getting instance from db: ", err)
		return err
	}

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{
		State:      int32(db.InstanceStateDELETING),
		ResourceID: req.Id,
	})
	if err != nil {
		log.Error("error updating instance state:", err)
		return err
	}

	log.Info("deleting instance")
	resp, err := SendEvent(node.Hostname, &rhyzome_protos.LibvirtEvent{
		DeleteInstance: &rhyzome_protos.DeleteInstance{
			ResourceId:   req.Id,
			RootVolumeId: instance.RootVolume,
		},
	})
	if err != nil {
		dberr := queries.SetInstanceState(ctx, db.SetInstanceStateParams{
			State:      int32(db.InstanceStateERROR),
			ResourceID: req.Id,
		})
		if dberr != nil {
			log.Error("error updating instance state:", err)
		}
		return err
	}

	if resp.Type == rhyzome_protos.LibvirtEventUpdate_FAILED {
		log.Warn("failed to delete instance: ", resp.Message)
		err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{
			State:      int32(db.InstanceStateERROR),
			ResourceID: req.Id,
		})
		if err != nil {
			log.Error("error updating instance state:", err)
			return err
		}
		return errors.New(resp.Message)
	}

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{
		State:      int32(db.InstanceStateDELETED),
		ResourceID: req.Id,
	})
	if err != nil {
		log.Error("error updating instance state")
		return err
	}

	log.Debug("instance deleted")

	return nil
}
