// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver_libvirt

import (
	"context"
	"database/sql"
	"errors"
	"io"
	"sync"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/peer"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-api/db"
	"git.callpipe.com/entanglement.garden/rhyzome-api/utils"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

var (
	clients          = make(map[string]*rhyzome_protos.RhyzomeLibvirt_EventStreamServer)
	clientsMux       = sync.Mutex{}
	eventsChannel    = make(map[string]chan *rhyzome_protos.LibvirtEventUpdate)
	eventsChannelMux = sync.Mutex{}
)

func EventStream(events rhyzome_protos.RhyzomeLibvirt_EventStreamServer) error {
	ctx := events.Context()
	p, ok := peer.FromContext(ctx)
	if !ok {
		log.Error("grpc method EventStream requested by client with no tls peer")
		return errors.New("tls is mandatory")
	}

	tlsInfo, ok := p.AuthInfo.(credentials.TLSInfo)
	if !ok {
		log.Error("failed to get tls credentials")
		return errors.New("tls is mandatory")
	}

	log.Debug("grpc client connected")

	for _, cert := range tlsInfo.State.PeerCertificates {
		log.Info("rpc connection from tls peer: ", cert.Subject, cert.DNSNames)
	}

	if len(tlsInfo.State.VerifiedChains) == 0 || len(tlsInfo.State.VerifiedChains[0]) == 0 {
		log.Warn("client certificate not verified")
		// reducing this to a warning until we can figure out why it's not populating.
		// return errors.New("certificate not verified")
	}

	for _, chains := range tlsInfo.State.VerifiedChains {
		for _, chain := range chains {
			log.Info("verified tls chain: ", chain.Subject)
		}
	}

	if len(tlsInfo.State.PeerCertificates) == 0 {
		log.Error("no peer certificate presented")
		return errors.New("no peer cert presented")
	}

	nodeName := tlsInfo.State.PeerCertificates[0].Subject.CommonName
	logger := log.WithField("node", nodeName)
	logger.Info("grpc connection from ", nodeName)

	clientsMux.Lock()
	_, ok = clients[nodeName]
	if ok {
		logger.Error("refusing duplicate connection from node ", nodeName)
		clientsMux.Unlock()
		return errors.New("duplicate connection from node not allowed")
	}
	clients[nodeName] = &events
	clientsMux.Unlock()

	defer func() {
		clientsMux.Lock()
		_, ok := clients[nodeName]
		if !ok {
			logger.Debug("EventStream endpoint closed with no clients for this nodename")
			clientsMux.Unlock()
			return
		}
		delete(clients, nodeName)
		clientsMux.Unlock()
	}()

	dbConn, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		logger.Error("error connecting to database: ", err)
		return err
	}
	defer dbConn.Close()

	queries := db.New(dbConn)
	_, err = queries.SetNodeAvailability(ctx, db.SetNodeAvailabilityParams{Available: true, Hostname: nodeName})
	if err != nil {
		logger.Error("error setting node available: ", err)
		return err
	}
	defer func() {
		// set unavailable in a background context in case the current context is cancelled
		_, err = queries.SetNodeAvailability(context.Background(), db.SetNodeAvailabilityParams{Available: false, Hostname: nodeName})
		if err != nil {
			logger.Warn("error setting node unavailable: ", err)
		}
	}()

	node, err := getOrAddNode(ctx, queries, nodeName)
	if err != nil {
		logger.Error("error getting node from db: ", err)
		return err
	}

	ctx = context.WithValue(ctx, contextKeyNode, node)
	go listen(ctx)
	for {
		update, err := events.Recv()
		if err != nil {
			if err == io.EOF {
				logger.Debug("got eof")
				return nil
			}
			if err == context.Canceled {
				logger.Debug("context canceled, stopping listener")
				return nil
			}
			logger.Warn("error receiving event from grpc client: ", err)
			// continue
			return err // until we can correctly detect context cancelled
		}
		eventLogger := logger.WithField("event_id", update.EventId)
		eventLogger.Debugf("received update from grpc client: %#v", update)
		eventsChannelMux.Lock()
		ch, ok := eventsChannel[update.EventId]
		if !ok {
			eventLogger.Debug("ignoring update for event ID with no listener")
			eventsChannelMux.Unlock()
			continue
		}
		ch <- update
		delete(eventsChannel, update.EventId)
		eventsChannelMux.Unlock()
		eventLogger.Debug("event delivered")
	}
}

func SendEvent(node string, event *rhyzome_protos.LibvirtEvent) (*rhyzome_protos.LibvirtEventUpdate, error) {
	if event.EventId == "" {
		event.EventId = uuid.New().String()
	}

	clientsMux.Lock()
	c, ok := clients[node]
	if !ok {
		clientsMux.Unlock()
		return nil, errors.New("node not connected")
	}

	ch := make(chan *rhyzome_protos.LibvirtEventUpdate)
	eventsChannelMux.Lock()
	// hope we dont get duplicate event IDs
	eventsChannel[event.EventId] = ch
	eventsChannelMux.Unlock()

	log.WithField("event_id", event.EventId).Debug("sending event to ", node)
	err := (*c).Send(event)
	if err != nil {
		clientsMux.Unlock()
		return nil, err
	}
	clientsMux.Unlock()

	return <-ch, nil
}

func getOrAddNode(ctx context.Context, queries *db.Queries, hostname string) (db.Node, error) {
	node, err := queries.GetNodeByHostname(ctx, hostname)
	if err == sql.ErrNoRows {
		node, err = addNode(ctx, queries, hostname)
	}
	if err != nil {
		return db.Node{}, err
	}
	return node, nil
}

func addNode(ctx context.Context, queries *db.Queries, hostname string) (db.Node, error) {
	id := utils.GenerateID()
	logger := log.WithFields(log.Fields{"node_id": id, "hostname": hostname})
	logger.Info("registering new node")
	err := queries.CreateResource(ctx, db.CreateResourceParams{
		ResourceID: id,
		Tenant:     db.DefaultTenantName,
	})
	if err != nil {
		return db.Node{}, err
	}

	node, err := queries.AddNode(ctx, db.AddNodeParams{
		ResourceID: id,
		Hostname:   hostname,
		Available:  true,
	})
	if err != nil {
		return db.Node{}, err
	}

	return node, nil
}
