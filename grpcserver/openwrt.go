// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	grpcserver_openwrt "git.callpipe.com/entanglement.garden/rhyzome-api/grpcserver/openwrt"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

type openwrtServer struct {
	rhyzome_protos.UnimplementedRhyzomeOpenwrtServer
}

func (openwrtServer) EventStream(events rhyzome_protos.RhyzomeOpenwrt_EventStreamServer) error {
	return grpcserver_openwrt.EventStream(events)
}
