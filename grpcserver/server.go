// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/security/advancedtls"

	"git.callpipe.com/entanglement.garden/rhyzome-api/config"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

var (
	server      *grpc.Server
	certificate tls.Certificate
	ca          *x509.CertPool
)

func ListenAndServe() {
	ReloadCertificates()

	creds, err := advancedtls.NewServerCreds(&advancedtls.ServerOptions{
		IdentityOptions:   advancedtls.IdentityCertificateOptions{GetIdentityCertificatesForServer: getIdentityCertificatesForServer},
		RootOptions:       advancedtls.RootCertificateOptions{GetRootCertificates: getRootCertificates},
		RequireClientCert: true,
		VType:             advancedtls.CertAndHostVerification,
	})
	if err != nil {
		log.Error("failed to initial TLS credentials: ", err)
		return
	}

	server = grpc.NewServer(grpc.Creds(creds))

	lis, err := net.Listen("tcp", config.C.GRPCBind)
	if err != nil {
		log.Error("grpc server failed to listen:", err)
		return
	}

	rhyzome_protos.RegisterRhyzomeLibvirtServer(server, libvirtServer{})

	rhyzome_protos.RegisterRhyzomeOpenwrtServer(server, openwrtServer{})

	log.Info("Starting gRPC server on ", config.C.GRPCBind)
	err = server.Serve(lis)
	if err != nil {
		log.Fatal("error from grpc listener: ", err)
	}
}

func Shutdown() {
	log.Info("Shutting down gRPC server")
	server.Stop()
}

func getIdentityCertificatesForServer(clientHello *tls.ClientHelloInfo) ([]*tls.Certificate, error) {
	return []*tls.Certificate{&certificate}, nil
}

func getRootCertificates(params *advancedtls.GetRootCAsParams) (*advancedtls.GetRootCAsResults, error) {
	return &advancedtls.GetRootCAsResults{TrustCerts: ca}, nil
}

func ReloadCertificates() {
	newCa := x509.NewCertPool()
	bs, err := ioutil.ReadFile(config.C.PKI.CA)
	if err != nil {
		log.Fatal("failed to read client ca cert:", err)
		return
	}
	ok := newCa.AppendCertsFromPEM(bs)
	if !ok {
		log.Fatal("failed to append client certs")
		return
	}
	ca = newCa

	newCertificate, err := tls.LoadX509KeyPair(config.C.PKI.Cert, config.C.PKI.Key)
	if err != nil {
		log.Fatal("error loading grpc TLS key pair:", err)
		return
	}
	certificate = newCertificate

	log.Info("loaded TLS certificate ", config.C.PKI.Cert, " and key ", config.C.PKI.Key)
}
