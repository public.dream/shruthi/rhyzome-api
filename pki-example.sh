#!/usr/bin/env bash
set -euo pipefail

mkdir -m 0700 -p ~/.step/certificates
cd $_
echo -e "Generated certs will be stored in: $PWD \n"

echo "# 1. Create a root CA"
cat > ../root.tpl <<EOD
{
  "subject": {
    "commonName": "Example Root CA"
  },
  "issuer": {
    "commonName": "Example Root CA"
  },
  "keyUsage": ["certSign", "crlSign"],
  "basicConstraints": {
    "isCa": true,
    "maxPathLen": 2
  }
}
EOD
step certificate create --insecure --no-password --template ../root.tpl --kty OKP --curve Ed25519 "Example Root CA" root_ca.crt root_ca.key

echo "# 2. Create leaf certificate and bundle it"
step certificate create --insecure --no-password --ca root_ca.crt --ca-key root_ca.key "example" leaf.crt example.key
cat leaf.crt root_ca.crt > example.crt

echo "# 3. Verify certificate (returns zero for success)"
step certificate verify --roots root_ca.crt example.crt
if [ $? -eq 0 ];
then 
    echo "Success!"
else
    echo "Failed to verify certificate."
fi